seccure (0.5-2) unstable; urgency=medium

  * Fix insecure-copyright-format-uri
  * Refresh packaging (debhelper 12, std-ver 4.3.0)

 -- Tomasz Buchert <tomasz@debian.org>  Sat, 26 Jan 2019 13:06:00 +0100

seccure (0.5-1) unstable; urgency=medium

  * New maintainer. Thanks James! (Closes: #797688)
  * Imported Upstream version 0.5
  * d/patches: refresh
  * d/copyright: update & switch GPL-2+ to LGPL-3+

 -- Tomasz Buchert <tomasz@debian.org>  Thu, 03 Sep 2015 11:14:26 +0200

seccure (0.4-0.1) unstable; urgency=low

  * Non-Maintainer Upload
  * New upstream version (Closes: #671667)
   - upstream secure memory overhaul, dropping now-unneeded memlock patch
  * refresh patches
  * convert packaging to dh 9
  * convert to 3.0 (quilt) (Closes: #664390)
  * bump Standards-Version to 3.9.5 (no changes needed)
  * ensure use of LDFLAGS during compilation
  * added debian/watch
  * debian/copyright: convert to machine-readable format
  * run test suite

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 05 Feb 2014 00:19:00 -0500

seccure (0.3-3) unstable; urgency=low

  * Modify the patch added in the last upload to enable gcrypt's secure
    memory, as apparently not disabling it is different to enabling it.
    (Closes: #489835)

 -- James Westby <jw+debian@jameswestby.net>  Tue, 16 Sep 2008 14:41:18 +0100

seccure (0.3-2) unstable; urgency=low

  * Make failure to mlock all memory only a warning, and re-enable gcrypt's
    memory locking. Changes in pam since etch mean that the kernel's defaults
    for memory locking are now respected, and seccure tries to lock more
    memory than that, which means that the program can't be started. The patch
    is from the upstream author, who considers it a stop-gap, but doesn't
    anticipate having a proper fix before lenny. (Closes: #489835)
  * Don't ignore "$(MAKE) clean" errors. The normal reason for this is the use
    of autotools leading to the makefile not necessarily being present when
    the clean target is run, be this package doesn't use autotools, so that
    won't happen.
  * Add a description to debian/patches/10-man-hyphen.dpatch.
  * Update the standards version to 3.8.0 (no changes required).
  * Release team: this isn't the diff that I sent you, I apologise. My sponsor
    suggested that fixing these small things wouldn't be a problem.

 -- James Westby <jw+debian@jameswestby.net>  Wed, 06 Aug 2008 11:33:27 +0100

seccure (0.3-1) unstable; urgency=low

  * New upstream release.

 -- James Westby <jw+debian@jameswestby.net>  Thu,  7 Sep 2006 20:34:30 +0100

seccure (0.2-1) unstable; urgency=low

  * Initial release (Closes: #378987)

 -- James Westby <jw+debian@jameswestby.net>  Tue, 25 Jul 2006 03:42:20 +0100
